\documentclass[a4paper,twocolumn]{article}

\include{packages}
\include{config}

\begin{document}

\title{Mayan languages and Neural Machine Translation: A first approach}
\author{Andrés Lou}
\date{}

\maketitle

\section{Introduction: The Mayan languages} % (fold)
\label{sec:introduction}

The Mayan language family is a language family spoken in an area covering the modern states of Guatemala, Belize, and Southern Mexico \parencite[p 81]{law2014language}. The family consists of around 30 languages grouped into five or six major sub-groups, depending on the source \parencite{campbell1985mayan,law2014language}. Most subgroups and most speakers are found today in Guatemala, where between 40-60\% of the population speak a Mayan language natively \parencite{fischer1996introduction,england2003mayan}. We will focus almost exclusively on the case of the Mayan languages in Guatemala, with the occasional reference to the rest.

\subsection{Social context of the Mayan languages in Guatemala} % (fold)
\label{sub:social_context_of_the_mayan_languages_in_guatemala}

Indigenous peoples in Guatemala are almost exclusively Maya, with the sole exception of the Xinca people, who are culturally and linguistically distinct from the Maya. Mayan languages are held as the greatest expression of Mayan identity in the region: They are relatively healthy, especially when compared with other native languages in the Americas, as there are millions of speakers whose age groups are not confined to the elderly; there are active revitalisation and revival movements championed by the younger generations, and there is a growing community of academically trained researchers that for decades have produced most of the work in the field of Mayan linguistics \parencite{bricker2007quarter,england2003mayan}. Despite this, Mayan languages are considered to be somewhat in decline: according to \textcite{richards2003atlas}, only around half the population of ethnic Mayas are Mayan speakers, and the languages continue to be associated in many social contexts to such traits as backwardness, ignorance, poverty, etc \parencite{england2003mayan}.

There is a considerable body of work done on the sociolinguistics of the Mayan languages. \textcite{romero2017labyrinth} provides an excellent survey of the current literature, as well as presenting a picture of the nature of the two frameworks most commonly used: the Variationist approach, which focuses on the effects of society on language and naturally occurring speech, and the more traditional Sociology of language, which focuses on the effects of language on society and does not always consider language itself as the object of study. The latter approach is more relevant for our case study, as it provides the context that justifies the endeavour of applying Neural Machine Translation (NMT) to Mayan languages.

Much variation exists in terms of breadth and number of speakers of the different Mayan languages in Guatemala. The three most spoken, K'iche', Q'eqchi' and Kaqchikel, all have between a million and half a million native speakers, and are spoken across different geographical areas in the country  \parencite{richards2003atlas,law2014language}; in contrast, there are some languages with very few speakers left and in imminent danger of language death, as is the case with Itzaj, which only had a few dozen elderly speakers as of the late 1990s and was confined to a tiny region in the north shores of Lake Petén Itzá \parencite{hofling1996indigenous}. Despite their size in terms of speakers and geographical exten, however, Mayan languages are not given an official status anywhere in the country; their use is widespread in daily activities and familial environments, but they are almost all but absent in matters of governance, education, mass media and healthcare \parencite{romero2017labyrinth}; for example, at the time of this writing, the official online portal to register for the government-sponsored Covid-19 vaccination program is only accessible in Spanish \parencite{españa2021plan}. 

Bilingualism with Spanish is very common, though not uniform across geographical, population and gender lines: isolated communities often present a high number of monolingual speakers, and men usually exhibit higher proficiency in bilingualism \parencite{Bennett2015Intro,romero2017labyrinth,richards2003atlas}. Language shift, whereby a Mayan language is displaced by either Spanish or, less commonly, another Mayan language \parencite{Bennett2015Intro,romero2012they}, occurs more quickly in urban environments, where a lingua franca is expected to be used. Nevertheless, it is yet very common for the children of parents who do not actively pass down the language to assimilate a Mayan language from their grandparents and extended family. Additionally, in recent decades, there has been a general movement towards making Mayan languages more visible outside their historically confined spheres by speakers who make a point of using them publicly and in front of officials who otherwise do not speak the languages \parencite{little2009language}.

Modern Mayan languages are written using the Latin script, with additional diacritics used to denote features such as vowel length or glotalisation, amongst others. Literacy is overall low as a result of decades of policy where using native languages in schools was discouraged or even punished \parencite{french2010maya}. \textcite{romero2017labyrinth} indicates that such policies, along witih the expectation that any kind of indigenous education was to be aimed at increasing proficiency in Spanish, have caused many Mayan speakers to regard the orthography of their own languages as more difficult and inaccessible than that of Spanish. Change was slowly brought about with the introduction of bilingual educational programs in the early 1990s, which finally led to a continued effort to revitalise the role of written Mayan languages. The Academy of Mayan Languages of Guatemala (ALMG), established in 1990, plays an important role in the standarisation of both the orthography of the different “linguistic communities” and their corresponding spoken languages, while also engaging in literacy and publication efforts in Mayan languages \parencite{almg2021historia}

Mayan languages exhibit an unexpectedly high degree of dialectal variation \parencite{romero2017labyrinth}. While the most important reason for this is natural language change and historical innovation, the sprachbund of the Mayan languages in Guatemala has resulted in much linguistic exchange in the forms of loanwords and calques, usually manifesting as the influence of a language with more speakers and political leverage over that with fewer speakers and less influence. Dialectal divergence within the same language is also attested, as is the case with the Western and Eastern dialects of Q'equchi' \parencite{dechicchis1989q}. Additionally, the politics of identity play a key role in delimiting the difference between a language and a dialect in the mind of their speakers, as seen in the cases of Achi, Akatek, and Chalchitek, which are sometimes considered, somewhat controversially, dialects of K’iche’, Q’anjob’al, and Awakatek, respectively \parencite{Bennett2015Intro}. In general, Mayan languages exhibit limited mutual intelligibility, and code-switching with Spanish and other Mayan languages in areas of high contact is common \parencite{little2009language}.

\subsection{Mayan phonology, morphosyntax and semantics} % (fold)
\label{sub:mayan_phonology_syntax_and_semantics}
Here we present a extremely condensed overview of the linguistics of Mayan languages for those unfamiliar with them. This section is mostly a summary of the extensive works by \textcite{Bennet2015phonology,coon2015morphosyntax,henderson2015semantics}.

\subsubsection{Phonology} % (fold)
\label{ssub:phonology}
Most Mayan languages distinguish ten vowel phonemes by contrasting /a, e, i, o, u/ as either short or long. Vowel length is usually conditioned by stress. Diphthongs are atypical and there is variation in the presence of hiatus. Vowel-initial words are rare and are usually avoided by inserting a glotal stop, \textipa{[\textglotstop]}. Unstressed vowels tend to be deleted.

\textcite{Bennet2015phonology} presents in detail the consonant inventory of modern-day Mayan languages, along with a much more detailed overview of the shared phonology of the language family. A salient trait of Mayan languages that has been widely studied is the contrast of voiceless stops with glottalised stops at the same place of articulation (eg /p/ and /p'/, /k/ and /k'/). 

Stress is present both fixed and mobile, though it is only phonemically contrasted in the Chontal language. Incipient instances of contrastive tone is reported in some languages.

\subsubsection{Morphosyntax} % (fold)
\label{ssub:morphosyntax}

Mayan languages are ergative-absolutive, which means the subject of an intransitive verb is syntactically marked like the object of a transitive verb; as a result, the “subjects” of transitive and intransitive verbs are not identical, which stands in contrast with languages that follow a nominative-accusative alignment, like Spanish. Classifier systems abound throughout the family and can either modify noun or numerals.

Word order is verb-initial, but there is room for variation for the purposes of pragmatics and discourse. Pro-drop, where some classes of pronouns are omitted if they are inferable, is allowed and plays an important role in determining word order. \textcite{coon2015morphosyntax} presents a comprehensive summary of many other features that require more in-depth analysis and training.

\subsubsection{Semantics} % (fold)
\label{ssub:semantics}

Verb roots in Mayan languages are usually polycategorical, meaning they may accept transitive, intransitive or positional derivations. Questions are formed with either intonation, question particles, or interrogative pronouns. Topicalisation and focus often trigger different word order than the neutral verb-initial arrangement and they might also use a focus particle, depending on whether the focus is informational or identificational. \textcite{henderson2015semantics} provides a much more in-depth and relatively accessible description of the work done with more complex subjects.

\section{Neural Machine Translation of low-resource languages} % (fold)
\label{sec:neural_machine_translation_of_low_resource_languages}
Unprecedented advances in the field of Neural Machine Translation (NMT) have been achieved in the last three years due to the rise of the Transformer \parencite{vaswani2017attention}. However, neural models have the considerable caveat of requiring enormous amounts of data in order to reach such performances. As is to be expected, some languages have access to more data than others, particularly English, whose presence and reach in all forms of modern-day media dwarf all others \parencite{bender2019rule}. It is notable that the number of speakers alone does not determine whether a language, on its own, will have access to the volume of data required to produce high-performing NMT models; the top ten most spoken languages in the world by native speakers (Mandarin, Spanish, English, Hindi, Arabic, Portuguese, Bengali, Russian, Japanese, and Punjabi) all boast dozens, or even hundreds, of millions of speakers. Yet as of this writing, the best performing NMT models all involve the pairings produced by the languages of developed countries (where most of the research takes place), or countries where there exist a significant body of written media. Thus, the best performing models usually include language pairs drawn from English, French, German, Spanish, Mandarin and Hindi, with a disproportionate share of the work including English \parencite{bender2019rule}.

The term \textit{low-resource language} (LRL) can apply to more than one scenario. A language is considered low-resource if, for example, it is threatened or dying, as is the case of Itzaj; additionally, a language might not be in danger of death or extinction but might be relatively confined to a small geographical area, as is the case of Kaqchikel; finally, a language might have a relatively strong presence in terms of number of speakers and geographical spread yet still have little in the way of written and recorded media, as is the case with K'iche' and Q'eqchi', the most spoken and most widely spread Mayan languages in Guatemala, respectively. All Mayan languages are low-resource, as, indeed, are most languages in the world.

The concept of LRLs is also dependent on the downstream task that involves a particular language. Different tasks, as well as different models, require different amounts of labelled data. The challenge involved in labelling the data is also conditioned on the task as well; for example, the labour involved in annotating a polarity classification dataset is not the same as annotating a dataset for POS tagging. The number of training instances can thus vary considerable, as shown in \parencite{hedderich-etal-2021-survey}.

The push for work on LRLs is an important direction in which NLP is moving. According to \textcite{ruder2019four}, it is currently one of the four biggest open problems in the field \footnote{The other three being Natural Language Understanding, reasoning about large or multiple documents, and datasets, problems and evaluations.}. Many approaches have been proposed to address the problem. \textcite{hedderich-etal-2021-survey} presents a survey of the most widely used methods, which we summarise here:

\label{generating_additional_labelled_data}
\textit{Generating data}. Each downstream task in NLP requires a specific kind of labelled data to carry out its training and testing. Several techniques are currently used for the purpose of creating synthetic data that enhance the limited resources, including Data Augmentation, where existing features are modified to create new instances with the same label; Distant and Weak Supervision \parencite{mintz-etal-2009-distant}, where labels are obtained from external sources, such as gazetteers or dictionaries; Cross-Lingual Annotation Projections \parencite{yarowsky-etal-2001-inducing}, where labels obtained from a pre-trained classifier in a high-resource language are projected onto a LRL using parallel corpora; Learning with Noisy Labels, where additional noise-reducing methods are put in place to deal with synthetic data; and Non-Expert Support, where the aid of non-expert annotators is sought to label data.

\label{transfer_learning}
\textit{Transfer learning}. Using pre-trained models, it is possible to further their training using task-specific data. This approach has become very common by leveraging the power of language models such as BERT \parencite{devlin-etal-2019-bert} and its multilingual capabilities,  and fine-tuning them using low-resource data. Transfer learning techniques include pre-trained language representations, (eg word2vec \parencite{Mikolov2013EfficientEO}, Byte Pair Encoding \parencite{sennrich2016neural}), and multilingual language models, where both high-resource and low-resource languages are trained using mononlingual data.

\section{Related work} % (fold)
\label{sec:related_work}
African languages exist in a similar, albeit superlative, situation; there are thousands of them, they boast millions of speakers, they serve as an important symbol of culture, they are a widely used tool for cultural exchange, and they are nevertheless poorly represented in NLP. The Masakhane project \parencite{Orife2020MasakhaneM} aims a addressing this situation by forming a community of researchers and non-researchers alike intent on strengthening the development of NLP of African languages. \textcite{martinus2019focus} describes a number of challenges the African NLP community faces:
\begin{itemize}
	\item Little official support for African indigenous languages
	\item Lack of resources for any kind of NLP task, and when those resources exist, they are hard to find
	\item Lack of benchmarks
	\item Low reproducibility
\end{itemize}

% Colonialism is a bitch
It should be immediately apparent that Mayan languages, and Indigenous American languages in general, face the same problems. Related to the problematisation of African languages and NLP, \textcite{abbott2019benchmarking} present the benchmarking of NMT models trained on set of South African languages as one of the first steps towards normalising the work in the field. A list of the most recent work done in NLP of African languages can be found in \parencite{DBLP:journals/corr/abs-2011-10361}. \textcite{goyal2021flores} introduced the FLORES-101 Evaluation Benchmark, consisting of a set of 3001 sentences, covering a wide array of domains, and professionally translated into 101 languages, though, crucially, they do not include any native language from the Americas. Nevertheless, the work on NLP of Indigenous languages of the Americas continues \parencite{mager2021proceedings}. Of note is the work by \textcite{tyers2021corpus} and \textcite{tyers2021survey}, who focus specifically on K'iche' and develop an annotated corpus for morphosyntactic structure and perform a survey of POS tagging methods respectively. Resources focused on parallel corpora of Mayan languages are scarce, though not non-existent: \textcite{christodouloupoulos2015massively} include K'iche' and Q'eqchi' as part of their project of presenting the Bible as a collection of large parallel corpora, and several published works by the ALMG contain thousands of parallel instances of a Mayan language and Spanish, as is the case with their K'iche' and Q'eqchi' vocabularies \parencite{delgado2010vocabulario,caal2004vocabulario}.

% section related_work (end)

\section{The corpora} % (fold)
\label{sec:the_corpora}
This is an incomplete list of the parallel corpora we have gathered so far:

\textit{The Bible}. The Bible in 100 languages project \parencite{christodouloupoulos2015massively} includes the entire Bible for Spanish and Q'eqchi', and the New Testament for K'iche', Kaqchikel, Jakaltek, Mam, and Uspantek, along with other languages, most of them non-Indo-European.

\textit{K'iche' and Q'eqchi' vocabularies}. The ALMG has published a number of vocabularies for many of the linguistic communities in Guatemala, including K'iche' \parencite{delgado2010vocabulario} and Q'eqchi' \parencite{caal2004vocabulario}. These vocabularies include some two to three thousand entries, each consisting of a term in the Mayan language, a Spanish translation, a sentence exemplifying the use of the Mayan term, and the Spanish translation of the sentence.

\textit{K'iche' corpus annotated for morphosyntax analysis}. \textcite{tyers2021corpus} introduced an annotated corpus of 1,433 sentences annotated with several morphosyntactical constituents. Each entry in the corpus also included the original raw text in K'iche' and its Spanish translation.


% section the_corpora (end)

\printbibliography

\end{document}